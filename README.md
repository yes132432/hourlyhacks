# HourlyHacks

HourlyHacks is a collaborative coding platform where users can add code to a shared canvas once every hour. The canvas updates automatically with each new contribution, creating a dynamic and constantly evolving showcase of code snippets.
How it works

To contribute to the canvas, simply go to the HourlyHacks website at https://yes132432.gitlab.io/hourlyhacks/ and enter your code snippet in the code editor. You can submit one code snippet per hour.

Each hour of the day has its own file on the canvas branch of the HourlyHacks Gitlab repository. When you submit a code snippet, it will be added to the file corresponding to the current hour.
Getting started

To run the HourlyHacks app locally, follow these steps:

    Clone the HourlyHacks Gitlab repository to your local machine.
    Install the necessary dependencies by running npm install.
    Start the development server by running npm start.
    Open the app in your web browser at http://localhost:3000.

# Contributing

If you're interested in contributing to HourlyHacks, feel free to submit a pull request or open an issue on the Gitlab repository. We're always looking for ways to improve the app and make it more user-friendly.
License

HourlyHacks is released under the MIT License. See the LICENSE file for more information.
Acknowledgements

HourlyHacks was created by Cassidy Dev.